# git-imc

import java.util.Scanner;

class CalculoImc {
    public static void main(String[] args) {
        System.out.println(" Faça seu Cálculo de IMC e saiba se você está acima ou abaixo do peso !");
            Scanner leitor = new Scanner(System.in);

        System.out.println("Digite o seu peso:");
        double peso = leitor.nextDouble();
        
        System.out.println("Digite sua altura:");
        double altura = leitor.nextDouble();
        
        double imc;

        imc = peso / (altura * altura);
        System.out.println("Seu IMC é: " + imc);
        
        if (imc <=18.5) {
            System.out.println("Você está MAGRO!");
        }
        if (18.5 < imc && imc <=24.9) {
            System.out.println("Você está NORMAL!");            
        }
        if (25 < imc && imc <=29.9) {
            System.out.println("Você está com SOBREPESO!");         
        }    
        if (imc >30) {
            System.out.println("Você está OBESO!");
    }
  }
}